﻿namespace SecondMonitor.Timing.Presentation.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using Commands;
    using Contracts.Commands;
    using Contracts.SimSettings;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;

    using PluginManager.GameConnector;

    using DataModel.Extensions;
    using DataModel.Snapshot.Drivers;
    using NLog;
    using PitBoard.Controller;
    using Rating.Application.Championship.ViewModels.IconState;
    using Rating.Application.Rating.ViewModels;
    using ReportCreation;
    using SecondMonitor.Timing.LapTimings.ViewModel;
    using SecondMonitor.Timing.SessionTiming.Drivers.Presentation.ViewModel;
    using SecondMonitor.Timing.SessionTiming.ViewModel;
    using SessionTiming;
    using ViewModels;
    using ViewModels.CarStatus;
    using ViewModels.TrackInfo;

    using SessionTiming.Drivers;
    using TrackRecords.Controller;
    using ViewModels.CarStatus.FuelStatus;
    using ViewModels.Controllers;
    using ViewModels.Factory;
    using ViewModels.SessionEvents;
    using ViewModels.Settings;
    using ViewModels.Settings.Model;
    using ViewModels.Settings.ViewModel;
    using ViewModels.Track.SituationOverview.Controller;
    using ViewModels.TrackRecords;

    public class TimingApplicationViewModel : AbstractViewModel, ISimulatorDataSetViewModel,  IPaceProvider
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly DriverLapsWindowManager _driverLapsWindowManager;
        private readonly ISettingsProvider _settingsProvider;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly ISimSettingsFactory _simSettingsFactory;
        private readonly SessionTimingFactory _sessionTimingFactory;
        private FuelOverviewViewModel _fuelOverviewViewModel;

        private ICommand _resetCommand;

        private TimingDataViewModelResetModeEnum _shouldReset = TimingDataViewModelResetModeEnum.NoReset;

        private SessionTiming _sessionTiming;
        private SessionType _sessionType = SessionType.Na;
        private SimulatorDataSet _lastDataSet;
        private bool _isOpenCarSettingsCommandEnable;

        private bool _isNamesNotUnique;
        private string _notUniqueNamesMessage;
        private Stopwatch _notUniqueCheckWatch;

        private Task _refreshGuiTask;
        private Task _refreshBasicInfoTask;
        private Task _refreshTimingCircleTask;
        private Task _refreshTimingGridTask;

        private string _connectedSource;
        private DisplaySettingsViewModel _displaySettingsViewModel;
        private IRatingApplicationViewModel _ratingApplicationViewModel;
        private SituationOverviewController _situationOverviewController;


        public TimingApplicationViewModel(DriverLapsWindowManager driverLapsWindowManager, ISettingsProvider settingsProvider, ITrackRecordsController trackRecordsController,
            ISessionEventProvider sessionEventProvider, ISimSettingsFactory simSettingsFactory, PitBoardController pitBoardController, SessionTimingFactory sessionTimingFactory, SituationOverviewController situationOverviewController, IViewModelFactory viewModelFactory)
        {
            PitBoardController = pitBoardController;
            SituationOverviewController = situationOverviewController;
            SituationOverviewController.PropertyChanged += SituationOverviewControllerOnPropertyChanged;
            TimingDataGridViewModel = viewModelFactory.Create<TimingDataGridViewModel>();
            SessionInfoViewModel = new SessionInfoViewModel();
            TrackInfoViewModel = new TrackInfoViewModel();
            _driverLapsWindowManager = driverLapsWindowManager;
            _settingsProvider = settingsProvider;
            _sessionEventProvider = sessionEventProvider;
            _sessionEventProvider.PlayerFinishStateChanged += SessionEventProviderOnPlayerFinishStateChanged;
            _simSettingsFactory = simSettingsFactory;
            _sessionTimingFactory = sessionTimingFactory;
            DoubleLeftClickCommand = new RelayCommand(OpenSelectedDriversLaps);
            DisplaySettingsViewModel = settingsProvider.DisplaySettingsViewModel;
            TrackRecordsViewModel = trackRecordsController.TrackRecordsViewModel;
        }

        public event EventHandler<SessionSummaryEventArgs> SessionCompleted;
        public event EventHandler<SessionSummaryEventArgs> PlayerFinished;

        public TimeSpan? PlayersPace => SessionTiming?.Player?.Pace;
        public TimeSpan? LeadersPace => SessionTiming?.Leader?.Pace;

        public PitBoardController PitBoardController { get; }

        public DisplaySettingsViewModel DisplaySettingsViewModel
        {
            get => _displaySettingsViewModel;
            private set
            {
                _displaySettingsViewModel = value;
                NotifyPropertyChanged();
                DisplaySettingsChanged();
            }
        }

        private SessionOptionsViewModel _currentSessionOptionsView;
        public SessionOptionsViewModel CurrentSessionOptionsView
        {
            get => _currentSessionOptionsView;
            set
            {
                SetProperty(ref _currentSessionOptionsView, value);
                ChangeOrderingMode();
                ChangeTimeDisplayMode();
            }
        }

        public FuelOverviewViewModel FuelOverviewViewModel
        {
            get => _fuelOverviewViewModel;
            set => SetProperty(ref _fuelOverviewViewModel, value);
        }
        public bool IsNamesNotUnique
        {
            get => _isNamesNotUnique;
            private set => SetProperty(ref _isNamesNotUnique, value);
        }

        public string NotUniqueNamesMessage
        {
            get => _notUniqueNamesMessage;
            private set => SetProperty(ref _notUniqueNamesMessage, value);
        }

        public ITrackRecordsViewModel TrackRecordsViewModel { get; }

        public TimingDataGridViewModel TimingDataGridViewModel { get; }

        public int SessionCompletedPercentage => _sessionTiming?.SessionCompletedPerMiles ?? 50;

        public ICommand ResetCommand => _resetCommand ?? (_resetCommand = new NoArgumentCommand(ScheduleReset));

        public ICommand OpenSettingsCommand { get; set; }

        public ICommand RightClickCommand { get; set; }

        public ICommand ScrollToPlayerCommand { get; set; }

        public ICommand OpenCurrentTelemetrySession { get; set; }

        public ICommand OpenCarSettingsCommand { get; set; }

        public ICommand OpenChampionshipWindowCommand { get; set; }

        public ChampionshipIconStateViewModel ChampionshipIconStateViewModel { get; set; }

        public bool IsOpenCarSettingsCommandEnable
        {
            get => _isOpenCarSettingsCommandEnable;
            set
            {
                _isOpenCarSettingsCommandEnable = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand DoubleLeftClickCommand
        {
            get;
            set;
        }

        public string SessionTime => _sessionTiming?.SessionTime.FormatToDefault() ?? string.Empty;

        public string ConnectedSource
        {
            get => _connectedSource;
            set => SetProperty(ref _connectedSource, value);
        }

        public string SystemTime => DateTime.Now.ToString("HH:mm");

        public string Title
        {
            get
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                string version = AssemblyName.GetAssemblyName(assembly.Location).Version.ToString();
                return "Second Monitor (Timing)(v" + version + " )";
            }
        }

        public SessionInfoViewModel SessionInfoViewModel { get; }

        public TrackInfoViewModel TrackInfoViewModel { get; }

        public SituationOverviewController SituationOverviewController
        {
            get => _situationOverviewController;
            set => SetProperty(ref _situationOverviewController, value);
        }

        public Dispatcher GuiDispatcher { get; set; }

        private DriverTimingViewModel _selectedDriverTimingViewModel;
        public DriverTimingViewModel SelectedDriverTimingViewModel
        {
            get => _selectedDriverTimingViewModel;
            set => SetProperty(ref _selectedDriverTimingViewModel, value);
        }

        public DriverTimingViewModel SelectedDriverTiming => SelectedDriverTimingViewModel;

        public SessionTiming SessionTiming
        {
            get => _sessionTiming;
            private set => SetProperty(ref _sessionTiming, value);
        }

        public CarStatusViewModel CarStatusViewModel
        {
            get;
            private set;
        }

        public IRatingApplicationViewModel RatingApplicationViewModel
        {
            get => _ratingApplicationViewModel;
            set => SetProperty(ref _ratingApplicationViewModel, value);
        }

        private bool TerminatePeriodicTasks { get; set; }

        public ICommand OpenLastReportCommand { get; set; }
        public ICommand OpenReportFolderCommand { get; set; }

        public Dictionary<string, TimeSpan> GetPaceForDriversMap()
        {
            if (SessionTiming == null)
            {
                return new Dictionary<string, TimeSpan>();
            }

            return SessionTiming.Drivers.ToDictionary(x => x.Value.DriverId, x => x.Value.Pace);
        }


        public void TerminatePeriodicTask(List<Exception> exceptions)
        {
            TerminatePeriodicTasks = true;
            if (_refreshBasicInfoTask.IsFaulted && _refreshBasicInfoTask.Exception != null)
            {
                exceptions.AddRange(_refreshBasicInfoTask.Exception.InnerExceptions);
            }

            if (_refreshGuiTask.IsFaulted && _refreshGuiTask.Exception != null)
            {
                exceptions.AddRange(_refreshGuiTask.Exception.InnerExceptions);
            }

            if (_refreshTimingCircleTask.IsFaulted && _refreshTimingCircleTask.Exception != null)
            {
                exceptions.AddRange(_refreshTimingCircleTask.Exception.InnerExceptions);
            }

            if (_refreshTimingGridTask.IsFaulted && _refreshTimingGridTask.Exception != null)
            {
                exceptions.AddRange(_refreshTimingGridTask.Exception.InnerExceptions);
            }
        }

        public void ApplyDateSet(SimulatorDataSet data)
        {
            if (data == null)
            {
                return;
            }

            IsOpenCarSettingsCommandEnable = !string.IsNullOrWhiteSpace(data?.PlayerInfo?.CarName);
            ConnectedSource = _lastDataSet?.Source;
            if (_sessionTiming == null || data.SessionInfo.SessionType == SessionType.Na || data.SessionInfo.SessionPhase == SessionPhase.Unavailable)
            {
                return;
            }

            _lastDataSet = data;

            if (_sessionType != data.SessionInfo.SessionType)
            {
                _shouldReset = TimingDataViewModelResetModeEnum.Automatic;
                _sessionType = _sessionTiming.SessionType;
            }

            // Reset state was detected (either reset button was pressed or timing detected a session change)
            if (_shouldReset != TimingDataViewModelResetModeEnum.NoReset)
            {
                CheckAndNotifySessionCompleted();
                CreateTiming(data);
                _shouldReset = TimingDataViewModelResetModeEnum.NoReset;
            }

            try
            {
                CheckIdsUniques(data);
                _sessionTiming?.UpdateTiming(data);
                CarStatusViewModel?.PedalsAndGearViewModel?.ApplyDateSet(data);
                CarStatusViewModel?.UpdateTyresSlipInformation(data);
            }
            catch (DriverNotFoundException)
            {
                _shouldReset = TimingDataViewModelResetModeEnum.Automatic;
            }
        }

        public void DisplayMessage(MessageArgs e)
        {
            if (e.IsDecision)
            {
                if (MessageBox.Show(
                        e.Message,
                        "Message from connector.",
                        MessageBoxButton.YesNo,
                        MessageBoxImage.Information) == MessageBoxResult.Yes)
                {
                    e.Action();
                }
            }
            else
            {
                MessageBox.Show(e.Message, "Message from connector.", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        public void Reset()
        {
            CarStatusViewModel = new CarStatusViewModel(this, _settingsProvider, _simSettingsFactory, _sessionEventProvider);
            ConnectedSource = "Not Connected";

            if (GuiDispatcher != null && GuiDispatcher.CheckAccess())
            {
                GuiDispatcher.Invoke(ScheduleRefreshActions);
            }
            else
            {
                ScheduleRefreshActions();
            }

            OnDisplaySettingsChange(this, null);
            _shouldReset = TimingDataViewModelResetModeEnum.NoReset;
            new AutoUpdateController().CheckForUpdate();
        }

        private void ScheduleRefreshActions()
        {
            _refreshGuiTask = SchedulePeriodicAction(() => RefreshGui(_lastDataSet), () => 2000, this, false);
            _refreshBasicInfoTask = SchedulePeriodicAction(() => RefreshBasicInfo(_lastDataSet), () => 100, this, false);
            _refreshTimingCircleTask = SchedulePeriodicAction(() => RefreshTimingCircle(_lastDataSet), () => 100, this, false);
            _refreshTimingGridTask = SchedulePeriodicAction(() => RefreshTimingGrid(_lastDataSet), () => DisplaySettingsViewModel.RefreshRate, this, false);
        }

        private void RefreshTimingGrid(SimulatorDataSet lastDataSet)
        {
            if (lastDataSet == null)
            {
                return;
            }
            TimingDataGridViewModel.UpdateProperties(lastDataSet);
        }

        private void PaceLapsChanged()
        {
            if (_sessionTiming != null)
            {
                _sessionTiming.PaceLaps = DisplaySettingsViewModel.PaceLaps;
            }

        }

        private void CheckIdsUniques(SimulatorDataSet dataSet)
        {
            if (_notUniqueCheckWatch == null || _notUniqueCheckWatch.ElapsedMilliseconds < 10000)
            {
                return;
            }

            List<IGrouping<string, string>> namesGrouping = dataSet.DriversInfo.Select(x => x.DriverSessionId).GroupBy(x => x).ToList();

            List<string> uniqueNames = namesGrouping.Where(x => x.Count() == 1).SelectMany(x => x).ToList();
            List<string> notUniqueNames = namesGrouping.Where(x => x.Count() > 1).Select(x => x.Key).ToList();


            if (notUniqueNames.Count == 0)
            {
                IsNamesNotUnique = false;
                return;
            }

            IsNamesNotUnique = true;
            NotUniqueNamesMessage = $"Not All Driver Ids are unique: Number of unique drivers - {uniqueNames.Count}, Not unique names - {string.Join(", ", notUniqueNames)} ";
            _notUniqueCheckWatch.Restart();
        }

        private void ScheduleReset()
        {
            _shouldReset = TimingDataViewModelResetModeEnum.Manual;
        }

        private void ChangeOrderingMode()
        {
            if (_sessionTiming == null)
            {
                return;
            }

            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(ChangeOrderingMode);
                return;
            }

            var mode = GetOrderTypeFromSettings();
            TimingDataGridViewModel.DriversOrdering = mode;
            _sessionTiming.DisplayGapToPlayerRelative = mode != DisplayModeEnum.Absolute;

        }

        private void ChangeTimeDisplayMode()
        {
            if (_sessionTiming == null || GuiDispatcher == null)
            {
                return;
            }

            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(ChangeTimeDisplayMode);
                return;
            }

            var mode = GetTimeDisplayTypeFromSettings();
            _sessionTiming.DisplayBindTimeRelative = mode == DisplayModeEnum.Relative;
            _sessionTiming.DisplayGapToPlayerRelative = mode == DisplayModeEnum.Relative;
        }

        private DisplayModeEnum GetOrderTypeFromSettings()
        {
            return CurrentSessionOptionsView.OrderingMode;
        }

        private DisplayModeEnum GetTimeDisplayTypeFromSettings()
        {
            return CurrentSessionOptionsView.TimesDisplayMode;
        }

        private SessionOptionsViewModel GetSessionOptionOfCurrentSession(SimulatorDataSet dataSet)
        {
            if (dataSet == null)
            {
                return new SessionOptionsViewModel();
            }

            switch (dataSet.SessionInfo.SessionType)
            {
                case SessionType.Practice:
                case SessionType.WarmUp:
                    return DisplaySettingsViewModel.PracticeSessionDisplayOptionsView;
                case SessionType.Qualification:
                    return DisplaySettingsViewModel.QualificationSessionDisplayOptionsView;
                case SessionType.Race:
                    return DisplaySettingsViewModel.RaceSessionDisplayOptionsView;
                default:
                    return new SessionOptionsViewModel();
            }
        }

        private void RefreshTimingCircle(SimulatorDataSet data)
        {
            if (data == null)
            {
                return;
            }
            SituationOverviewController.ApplyDateSet(data);
        }

        private void RefreshBasicInfo(SimulatorDataSet data)
        {
            if (data == null)
            {
                return;
            }

            NotifyPropertyChanged(nameof(SessionTime));
            NotifyPropertyChanged(nameof(SystemTime));
            NotifyPropertyChanged(nameof(SessionCompletedPercentage));
            CarStatusViewModel.ApplyDateSet(data);
            TrackInfoViewModel.ApplyDateSet(data);
            SessionInfoViewModel.ApplyDateSet(data);
        }

        private void SessionTimingDriverRemoved(object sender, DriverListModificationEventArgs e)
        {
            if (TerminatePeriodicTasks)
            {
                return;
            }
            TimingDataGridViewModel.RemoveDriver(e.Data);

            GuiDispatcher?.Invoke(() =>
            {
                SituationOverviewController.RemoveDriver(e.Data.DriverInfo);
            });

        }

        private void SessionTimingDriverAdded(object sender, DriverListModificationEventArgs e)
        {

            TimingDataGridViewModel.AddDriver(e.Data);
            SituationOverviewController.AddDriver(e.Data.DriverInfo);
            _driverLapsWindowManager.Rebind(TimingDataGridViewModel.DriversViewModels.FirstOrDefault(x => x.DriverShortName == e.Data.DriverShortName));
        }

        private void RefreshGui(SimulatorDataSet data)
        {
            if (data == null)
            {
                return;
            }

            ScrollToPlayerCommand?.Execute(null);
        }

        private void CreateTiming(SimulatorDataSet data)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => CreateTiming(data));
                return;
            }

            if (SessionTiming != null)
            {
                SessionTiming.DriverAdded -= SessionTimingDriverAdded;
                SessionTiming.DriverRemoved -= SessionTimingDriverRemoved;
                SessionTiming.LapCompleted -= SessionTimingOnLapCompleted;
            }

            bool invalidateLap = _shouldReset == TimingDataViewModelResetModeEnum.Manual ||
                                data.SessionInfo.SessionType != SessionType.Race;
            _lastDataSet = data;
            SessionTiming = _sessionTimingFactory.Create(data, this, invalidateLap);
            SessionInfoViewModel.SessionTiming = _sessionTiming;
            SessionTiming.DriverAdded += SessionTimingDriverAdded;
            SessionTiming.DriverRemoved += SessionTimingDriverRemoved;
            SessionTiming.LapCompleted += SessionTimingOnLapCompleted;
            SessionTiming.PaceLaps = DisplaySettingsViewModel.PaceLaps;

            CarStatusViewModel.Reset();
            TrackInfoViewModel.Reset();
            SituationOverviewController.Reset();
            PitBoardController.Reset();


            InitializeGui(data);
            ChangeTimeDisplayMode();
            ChangeOrderingMode();
            ConnectedSource = data.Source;
            _notUniqueCheckWatch = Stopwatch.StartNew();
            NotifyPropertyChanged(nameof(ConnectedSource));
        }

        private void SessionTimingOnLapCompleted(object sender, LapEventArgs e)
        {
            try
            {
                if (e.Lap.Driver.IsPlayer)
                {
                    PitBoardController.OnPlayerCompletedLap(_sessionEventProvider.LastDataSet, _sessionTiming.Drivers.Values);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public void StartNewSession(SimulatorDataSet dataSet)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => StartNewSession(dataSet));
                return;
            }

            CheckAndNotifySessionCompleted();
            if (dataSet.SessionInfo.SessionType == SessionType.Na)
            {
                return;
            }
            SessionInfoViewModel.Reset();
            UpdateCurrentSessionOption(dataSet);
            CreateTiming(dataSet);
        }

        private void UpdateCurrentSessionOption(SimulatorDataSet data)
        {
            CurrentSessionOptionsView = GetSessionOptionOfCurrentSession(data);
        }

        private void InitializeGui(SimulatorDataSet data)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => InitializeGui(data));
                return;
            }

            if (data.SessionInfo.SessionType != SessionType.Na)
            {
                TimingDataGridViewModel.MatchDriversList(_sessionTiming.Drivers.Values.ToList());
                _driverLapsWindowManager.RebindAll(TimingDataGridViewModel.DriversViewModels);
            }

            SituationOverviewController.ApplyDateSet(data);
        }

        private void OnDisplaySettingsChange(object sender, PropertyChangedEventArgs args)
        {
            ApplyDisplaySettings(DisplaySettingsViewModel);
            switch (args?.PropertyName)
            {
                case nameof(DisplaySettingsViewModel.PaceLaps):
                    PaceLapsChanged();
                    break;
                case nameof(SessionOptionsViewModel.OrderingMode):
                    ChangeOrderingMode();
                    break;
                case nameof(SessionOptionsViewModel.TimesDisplayMode):
                    ChangeTimeDisplayMode();
                    break;
            }
        }

        private void ApplyDisplaySettings(DisplaySettingsViewModel settingsView)
        {
            TrackInfoViewModel.TemperatureUnits = settingsView.TemperatureUnits;
            TrackInfoViewModel.DistanceUnits = settingsView.DistanceUnits;
        }

        private void DisplaySettingsChanged()
        {
            DisplaySettingsViewModel newDisplaySettingsViewModel = _displaySettingsViewModel;
            newDisplaySettingsViewModel.PropertyChanged += OnDisplaySettingsChange;
            newDisplaySettingsViewModel.PracticeSessionDisplayOptionsView.PropertyChanged += OnDisplaySettingsChange;
            newDisplaySettingsViewModel.RaceSessionDisplayOptionsView.PropertyChanged += OnDisplaySettingsChange;
            newDisplaySettingsViewModel.QualificationSessionDisplayOptionsView.PropertyChanged += OnDisplaySettingsChange;
        }

        private static async Task SchedulePeriodicAction(Action action, Func<int> delayFunc, TimingApplicationViewModel sender, bool captureContext)
        {
            while (!sender.TerminatePeriodicTasks)
            {
                await Task.Delay(delayFunc(), CancellationToken.None).ConfigureAwait(captureContext);

                if (!sender.TerminatePeriodicTasks)
                {
                    action();
                }
            }
        }

        private void SessionEventProviderOnPlayerFinishStateChanged(object sender, DataSetArgs e)
        {
            if (e.DataSet.SessionInfo.SessionType != SessionType.Race || e.DataSet.PlayerInfo.FinishStatus != DriverFinishStatus.Finished || _sessionTiming?.WasGreen != true)
            {
                return;
            }

            PlayerFinished?.Invoke(this, new SessionSummaryEventArgs(_sessionTiming.ToSessionSummary()));
        }

        private void CheckAndNotifySessionCompleted()
        {
            if (_sessionTiming?.WasGreen != true || _sessionTiming.IsFinished)
            {
                return;
            }

            _sessionTiming.Finish();
            SessionCompleted?.Invoke(this, new SessionSummaryEventArgs(_sessionTiming.ToSessionSummary()));
        }

        private void OpenSelectedDriversLaps()
        {
            _driverLapsWindowManager.OpenWindowDefault(SelectedDriverTiming);
        }

        private void SituationOverviewControllerOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SituationOverviewController.SituationOverviewViewModel))
            {
                NotifyPropertyChanged(nameof(SituationOverviewController));
            }
        }
    }
}
