﻿namespace SecondMonitor.Timing.SessionTiming
{
    using System.Collections.Generic;
    using Controllers;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using Drivers.ViewModel;
    using LapTimings;
    using NLog;
    using Presentation.ViewModel;
    using Rating.Application.Championship;
    using Rating.Application.Rating.RatingProvider;
    using Telemetry;
    using TrackRecords.Controller;
    using ViewModel;
    using ViewModels.SessionEvents;

    public class SessionTimingFactory
    {
        private readonly ISessionTelemetryControllerFactory _sessionTelemetryControllerFactory;
        private readonly IRatingProvider _ratingProvider;
        private readonly ITrackRecordsController _trackRecordsController;
        private readonly IChampionshipCurrentEventPointsProvider _championshipCurrentEventPointsProvider;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly DriverLapSectorsTrackerFactory _driverLapSectorsTrackerFactory;
        private readonly MapManagementController _mapManagementController;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public SessionTimingFactory(ISessionTelemetryControllerFactory sessionTelemetryControllerFactory, IRatingProvider ratingProvider, ITrackRecordsController trackRecordsController, IChampionshipCurrentEventPointsProvider championshipCurrentEventPointsProvider,
        ISessionEventProvider sessionEventProvider, DriverLapSectorsTrackerFactory driverLapSectorsTrackerFactory, MapManagementController mapManagementController)
        {
            _sessionTelemetryControllerFactory = sessionTelemetryControllerFactory;
            _ratingProvider = ratingProvider;
            _trackRecordsController = trackRecordsController;
            _championshipCurrentEventPointsProvider = championshipCurrentEventPointsProvider;
            _sessionEventProvider = sessionEventProvider;
            _driverLapSectorsTrackerFactory = driverLapSectorsTrackerFactory;
            _mapManagementController = mapManagementController;
        }

        public SessionTiming Create(SimulatorDataSet dataSet, TimingApplicationViewModel timingApplicationViewModel, bool invalidateFirstLap)
        {
            Logger.Info($"New Seesion Started :{dataSet.SessionInfo.SessionType.ToString()}");
            SessionTiming timing = new SessionTiming(timingApplicationViewModel, _sessionTelemetryControllerFactory.Create(dataSet), _ratingProvider, _trackRecordsController, _championshipCurrentEventPointsProvider, _sessionEventProvider, _driverLapSectorsTrackerFactory, dataSet, _mapManagementController);
            Dictionary<string, DriverTiming> drivers = new Dictionary<string, DriverTiming>();
            foreach (DriverInfo driverInfo in dataSet.DriversInfo)
            {
                if (drivers.ContainsKey(driverInfo.DriverSessionId))
                {
                    continue;
                }
                //drivers[driverInfo.DriverSessionId] = DriverTiming.FromModel(driverInfo, timing, _driverLapSectorsTrackerFactory, false);
                drivers[driverInfo.DriverSessionId] = DriverTiming.FromModel(driverInfo, timing, _driverLapSectorsTrackerFactory, invalidateFirstLap);
            }

            timing.SetDrivers(dataSet, drivers.Values);
            return timing;
        }
    }
}