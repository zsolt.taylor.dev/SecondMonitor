﻿namespace SecondMonitor.Timing.LapTimings.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using DataModel.Extensions;
    using View;
    using SessionTiming.Drivers.Presentation.ViewModel;
    using SimdataManagement.DriverPresentation;

    public class DriverLapsWindowManager
    {
        private readonly DriverPresentationsManager _driverPresentationsManager;
        private readonly List<DriverLapsWindow> _openedWindows = new List<DriverLapsWindow>();

        public DriverLapsWindowManager(DriverPresentationsManager driverPresentationsManager)
        {
            _driverPresentationsManager = driverPresentationsManager;
        }


        public void OpenWindowDefault(DriverTimingViewModel driverTiming )
        {
            OpenWindow(driverTiming);
        }

        private void OpenWindow(DriverTimingViewModel driverTiming)
        {
            if (driverTiming == null)
            {
                return;
            }

            DriverLapsWindow lapsWindow = new DriverLapsWindow()
                                              {
                                                  Owner = Application.Current.MainWindow,
                                                  WindowStartupLocation = WindowStartupLocation.CenterOwner,
                                              };
            new DriverLapsViewModel(driverTiming, lapsWindow, _driverPresentationsManager);
            _openedWindows.Add(lapsWindow);
            lapsWindow.Closed += LapsWindow_Closed;
            lapsWindow.Show();
        }

        private void LapsWindow_Closed(object sender, EventArgs e)
        {
            if (sender is DriverLapsWindow window)
            {
                _openedWindows.Remove(window);
            }
        }

        public void Rebind(DriverTimingViewModel driverTiming)
        {
            _openedWindows.FindAll(p => ((DriverLapsViewModel)p.DataContext).DriverTiming.DriverShortName == driverTiming.DriverShortName).ForEach(p => Rebind(p, driverTiming));
        }

        public void RebindAll(IEnumerable<DriverTimingViewModel> driverTimings)
        {
            driverTimings.ForEach(Rebind);
        }

        private void Rebind(DriverLapsWindow window, DriverTimingViewModel newViewModel)
        {
            if (!(window.DataContext is DriverLapsViewModel oldDriverLapsViewModel))
            {
                return;
            }
            oldDriverLapsViewModel.UnRegisterOnGui();
            window.DataContext = new DriverLapsViewModel(newViewModel, window, _driverPresentationsManager);
        }
    }
}