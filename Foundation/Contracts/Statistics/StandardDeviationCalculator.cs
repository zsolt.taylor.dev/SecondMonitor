﻿namespace SecondMonitor.Contracts.Statistics
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class StandardDeviationCalculator
    {
        public static double ComputeDeviation(ICollection<double> values)
        {
            double mean = values.Sum() / values.Count;

            var squaresQuery = values.Select(x => (x - mean) * (x - mean));
            double sumOfSquares = squaresQuery.Sum();

            return Math.Sqrt(sumOfSquares / values.Count());
        }
    }
}