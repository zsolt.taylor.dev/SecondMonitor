﻿namespace SecondMonitor.ViewModels.Track.SituationOverview
{
    using System.Windows.Input;

    public interface IMapSidePanelViewModel : IViewModel
    {
        ICommand DeleteMapCommand { get; set; }
        ICommand RotateMapLeftCommand { get; set; }
        ICommand RotateMapRightCommand { get; set; }
    }
}