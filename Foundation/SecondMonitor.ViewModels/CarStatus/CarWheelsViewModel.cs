﻿namespace SecondMonitor.ViewModels.CarStatus
{
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using DataModel.Snapshot.Systems;

    public class CarWheelsViewModel : AbstractViewModel, ISimulatorDataSetViewModel
    {
        private WheelStatusViewModel _leftFrontTyre;
        private WheelStatusViewModel _rightFrontTyre;
        private WheelStatusViewModel _leftRearTyre;
        private WheelStatusViewModel _rightRearTyre;

        public CarWheelsViewModel(SessionRemainingCalculator sessionRemainingCalculator, IPaceProvider paceProvider, WheelStatusViewModelFactory wheelStatusViewModelFactory )
        {
            LeftFrontTyre = wheelStatusViewModelFactory.Create(true, sessionRemainingCalculator, paceProvider);
            LeftRearTyre = wheelStatusViewModelFactory.Create(true, sessionRemainingCalculator, paceProvider);
            RightFrontTyre = wheelStatusViewModelFactory.Create(false, sessionRemainingCalculator, paceProvider);
            RightRearTyre = wheelStatusViewModelFactory.Create(false, sessionRemainingCalculator, paceProvider);
        }

        public CarWheelsViewModel(WheelStatusViewModelFactory wheelStatusViewModelFactory)
        {
            LeftFrontTyre = wheelStatusViewModelFactory.Create(true);
            LeftRearTyre = wheelStatusViewModelFactory.Create(true);
            RightFrontTyre = wheelStatusViewModelFactory.Create(false);
            RightRearTyre = wheelStatusViewModelFactory.Create(false);
        }

        public WheelStatusViewModel LeftFrontTyre
        {
            get => _leftFrontTyre;
            private set
            {
                _leftFrontTyre = value;
                NotifyPropertyChanged();
            }
        }

        public WheelStatusViewModel RightFrontTyre
        {
            get => _rightFrontTyre;
            private set
            {
                _rightFrontTyre = value;
                NotifyPropertyChanged();
            }
        }

        public WheelStatusViewModel LeftRearTyre
        {
            get => _leftRearTyre;
            private set
            {
                _leftRearTyre = value;
                NotifyPropertyChanged();
            }
        }

        public WheelStatusViewModel RightRearTyre
        {
            get => _rightRearTyre;
            private set
            {
                _rightRearTyre = value;
                NotifyPropertyChanged();
            }
        }

        public void UpdateTyresSlipInformation(SimulatorDataSet dataSet)
        {
            Wheels wheels = dataSet?.PlayerInfo?.CarInfo?.WheelsInfo;

            if (wheels == null)
            {
                return;
            }

            LeftFrontTyre.UpdateSlippingInformation(dataSet, wheels.FrontLeft);
            RightFrontTyre.UpdateSlippingInformation(dataSet, wheels.FrontRight);
            LeftRearTyre.UpdateSlippingInformation(dataSet, wheels.RearLeft);
            RightRearTyre.UpdateSlippingInformation(dataSet, wheels.RearRight);
        }



        public void ApplyDateSet(SimulatorDataSet dataSet)
        {
            Wheels wheels = dataSet?.PlayerInfo?.CarInfo?.WheelsInfo;

            if (wheels == null)
            {
                return;
            }

            LeftFrontTyre.ApplyWheelCondition(dataSet, wheels.FrontLeft);
            RightFrontTyre.ApplyWheelCondition(dataSet, wheels.FrontRight);
            LeftRearTyre.ApplyWheelCondition(dataSet, wheels.RearLeft);
            RightRearTyre.ApplyWheelCondition(dataSet, wheels.RearRight);
        }

        public void ApplyDateSet(DriverInfo playerInfo)
        {
            Wheels wheels = playerInfo.CarInfo?.WheelsInfo;

            if (wheels == null)
            {
                return;
            }

            LeftFrontTyre.ApplyWheelCondition(wheels.FrontLeft);
            RightFrontTyre.ApplyWheelCondition(wheels.FrontRight);
            LeftRearTyre.ApplyWheelCondition(wheels.RearLeft);
            RightRearTyre.ApplyWheelCondition(wheels.RearRight);
        }

        public void Reset()
        {
            LeftFrontTyre.Reset();
            RightFrontTyre.Reset();
            LeftRearTyre.Reset();
            RightRearTyre.Reset();
        }
    }
}