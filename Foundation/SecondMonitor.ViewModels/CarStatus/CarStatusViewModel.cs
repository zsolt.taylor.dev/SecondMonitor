﻿namespace SecondMonitor.ViewModels.CarStatus
{
    using Contracts.SimSettings;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using FuelStatus;
    using SessionEvents;
    using Settings;

    public class CarStatusViewModel : AbstractViewModel, ISimulatorDataSetViewModel
    {

        private readonly SimulatorDSViewModels _viewModels;

        private CarWheelsViewModel _playersWheelsViewModel;
        private CarSystemsViewModel _carSystemsViewModel;
        private DashboardViewModel _dashboardViewModel;

        private PedalsAndGearViewModel _pedalAndGearViewModel;

        public CarStatusViewModel(IPaceProvider paceProvider, ISettingsProvider settingsProvider, ISimSettingsFactory settingsFactory, ISessionEventProvider sessionEventProvider)
        {
            SessionRemainingCalculator sessionRemainingCalculator = new SessionRemainingCalculator(paceProvider);
            _viewModels = new SimulatorDSViewModels {new CarWheelsViewModel(sessionRemainingCalculator, paceProvider, new WheelStatusViewModelFactory(settingsProvider)), new PedalsAndGearViewModel(settingsProvider), new CarSystemsViewModel(settingsProvider, settingsFactory, sessionEventProvider), new DashboardViewModel(settingsFactory, sessionEventProvider)};
            RefreshProperties();
        }



        public PedalsAndGearViewModel PedalsAndGearViewModel
        {
            get => _pedalAndGearViewModel;
            set => SetProperty(ref _pedalAndGearViewModel, value);
        }

        public CarWheelsViewModel PlayersWheelsViewModel
        {
            get => _playersWheelsViewModel;
            private set => SetProperty(ref _playersWheelsViewModel, value);
        }

        public CarSystemsViewModel CarSystemsViewModel
        {
            get => _carSystemsViewModel;
            private set => SetProperty(ref _carSystemsViewModel, value);
        }

        public DashboardViewModel DashboardViewModel
        {
            get => _dashboardViewModel;
            set => SetProperty(ref _dashboardViewModel, value);
        }

        public void ApplyDateSet(SimulatorDataSet dataSet)
        {
            if (dataSet.SessionInfo.SessionType == SessionType.Na)
            {
                return;
            }

            _viewModels.ApplyDateSet(dataSet);

        }

        public void UpdateTyresSlipInformation(SimulatorDataSet dataSet)
        {
            if(dataSet == null)
            {
                return;
            }
            PlayersWheelsViewModel.UpdateTyresSlipInformation(dataSet);
        }

        public void Reset()
        {
            _viewModels.Reset();
        }

        private void RefreshProperties()
        {
            PlayersWheelsViewModel = _viewModels.GetFirst<CarWheelsViewModel>();
            PedalsAndGearViewModel = _viewModels.GetFirst<PedalsAndGearViewModel>();
            CarSystemsViewModel = _viewModels.GetFirst<CarSystemsViewModel>();
            DashboardViewModel = _viewModels.GetFirst<DashboardViewModel>();
        }
    }
}