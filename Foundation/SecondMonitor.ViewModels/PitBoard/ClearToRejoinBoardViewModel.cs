﻿namespace SecondMonitor.ViewModels.PitBoard
{
    public class ClearToRejoinBoardViewModel : AbstractViewModel
    {
        private string _driverName;
        private string _distance;
        private bool _isClearToRejoin;

        public ClearToRejoinBoardViewModel()
        {
            DriverName = "Fedor Predkozisko";
            Distance = "200 m";
            IsClearToRejoin = false;
        }

        public bool IsClearToRejoin
        {
            get => _isClearToRejoin;
            set => SetProperty(ref _isClearToRejoin, value);
        }

        public string DriverName
        {
            get => _driverName;
            set => SetProperty(ref _driverName, value);
        }

        public string Distance
        {
            get => _distance;
            set => SetProperty(ref _distance, value);
        }
    }
}