﻿namespace SecondMonitor.ViewModels.FuelConsumption
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using CarStatus;
    using CarStatus.FuelStatus;
    using Contracts.Commands;
    using Controllers;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Summary.FuelConsumption;
    using SessionEvents;

    public class FuelConsumptionController : IController
    {
        private static readonly TimeSpan MinimumSessionLength = TimeSpan.FromMinutes(2);
        private readonly FuelConsumptionRepository _fuelConsumptionRepository;
        private readonly Lazy<OverallFuelConsumptionHistory> _overallFuelHistoryLazy;
        private bool _isFuelCalculatorShown;
        private readonly FuelPlannerViewModelFactory _fuelPlannerViewModelFactory;
        private readonly ISessionEventProvider _sessionEventProvider;
        private SimulatorDataSet _lastDataSet;
        private bool _autoOpened;

        public FuelConsumptionController(FuelConsumptionRepository fuelConsumptionRepository, IPaceProvider paceProvider, FuelPlannerViewModelFactory fuelPlannerViewModelFactory, ISessionEventProvider sessionEventProvider)
        {
            _fuelConsumptionRepository = fuelConsumptionRepository;
            _fuelPlannerViewModelFactory = fuelPlannerViewModelFactory;
            _sessionEventProvider = sessionEventProvider;
            _overallFuelHistoryLazy = new Lazy<OverallFuelConsumptionHistory>(LoadOverallFuelConsumptionHistory);
            FuelOverviewViewModel = new FuelOverviewViewModel(new SessionRemainingCalculator(paceProvider))
            {
                IsVisible = true,
                ShowFuelCalculatorCommand = new RelayCommand(ShowFuelCalculator),
                HideFuelCalculatorCommand = new RelayCommand(HideFuelCalculator),
            };
        }

        public FuelOverviewViewModel FuelOverviewViewModel { get; }

        private OverallFuelConsumptionHistory OverallFuelConsumptionHistory => _overallFuelHistoryLazy.Value;

        public Task StartControllerAsync()
        {
            return Task.CompletedTask;
        }

        public Task StopControllerAsync()
        {
            StoreCurrentSessionConsumption();
            if (_overallFuelHistoryLazy.IsValueCreated)
            {
                _fuelConsumptionRepository.Save(OverallFuelConsumptionHistory);
            }
            return Task.CompletedTask;
        }

        public void ApplyDataSet(SimulatorDataSet dataSet)
        {
            _lastDataSet = dataSet;
            FuelOverviewViewModel.ApplyDateSet(dataSet);
            if (dataSet.SessionInfo.SessionType == SessionType.Race && dataSet.SessionInfo.SessionPhase == SessionPhase.Countdown && !_autoOpened)
            {
                AutoOpenFuelCalculator(dataSet);
            }

            if (_autoOpened && (dataSet.SessionInfo.SessionPhase != SessionPhase.Countdown || dataSet.SessionInfo.SessionType != SessionType.Race))
            {
                _autoOpened = false;
                HideFuelCalculator();
            }
        }

        public void Reset()
        {
            StoreCurrentSessionConsumption();
            FuelOverviewViewModel.Reset();
        }

        private void AutoOpenFuelCalculator(SimulatorDataSet dataSet)
        {
            _autoOpened = true;
            ShowFuelCalculator();
            if (!_sessionEventProvider.CurrentSimulatorSettings.IsSessionLengthAvailableBeforeStart)
            {
                return;
            }

            int totalMinutes = 0;
            int totalLaps = 2;
            switch (dataSet.SessionInfo.SessionLengthType)
            {
                case SessionLengthType.Na:
                    break;
                case SessionLengthType.Laps:
                    totalLaps += dataSet.SessionInfo.TotalNumberOfLaps;
                    break;
                case SessionLengthType.Time:
                    totalMinutes = (int)Math.Ceiling(dataSet.SessionInfo.SessionTimeRemaining / 60);
                    break;
                case SessionLengthType.TimeWithExtraLap:
                    totalMinutes = (int)Math.Ceiling(dataSet.SessionInfo.SessionTimeRemaining / 60);
                    totalLaps++;
                    break;
            }

            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.RequiredLaps = totalLaps;
            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.RequiredMinutes = totalMinutes;
        }

        private void ShowFuelCalculator()
        {
            if (_lastDataSet?.PlayerInfo == null)
            {
                return;
            }

            var fuelConsumptionEntries = OverallFuelConsumptionHistory.GetTrackConsumptionHistoryEntries(_lastDataSet.Source, _lastDataSet.SessionInfo.TrackInfo.TrackFullName, _lastDataSet.PlayerInfo.CarName);
            var currentSessionEntry = CreateCurrentSessionFuelConsumptionDto();
            if (currentSessionEntry != null)
            {
                fuelConsumptionEntries = new[] {currentSessionEntry}.Concat(fuelConsumptionEntries).ToList();
            }

            if (fuelConsumptionEntries.Count == 0)
            {
                return;
            }

            FuelOverviewViewModel.FuelPlannerViewModel = _fuelPlannerViewModelFactory.Create(fuelConsumptionEntries);
            IsFuelCalculatorShown = FuelOverviewViewModel.FuelPlannerViewModel.Sessions.Count != 0;
        }

        private void HideFuelCalculator()
        {
            IsFuelCalculatorShown = false;
        }

        private bool IsFuelCalculatorShown
        {
            get => _isFuelCalculatorShown;
            set
            {
                _isFuelCalculatorShown = value;
                FuelOverviewViewModel.IsVisible = !value;
                FuelOverviewViewModel.FuelPlannerViewModel.IsVisible = value;
            }
        }

        private void StoreCurrentSessionConsumption()
        {
            if (_lastDataSet?.PlayerInfo == null || FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime < MinimumSessionLength)
            {
                return;
            }

            OverallFuelConsumptionHistory.AddTrackConsumptionHistoryEntry(CreateCurrentSessionFuelConsumptionDto());
        }

        private SessionFuelConsumptionDto CreateCurrentSessionFuelConsumptionDto()
        {
            if (_lastDataSet?.PlayerInfo == null || FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime < MinimumSessionLength)
            {
                return null;
            }

            var currentSessionFuelConsumption = new SessionFuelConsumptionDto()
            {
                IsWetSession = FuelOverviewViewModel.IsWetSession,
                CarName = _sessionEventProvider.CurrentCarName,
                LapDistance = _lastDataSet.SessionInfo.TrackInfo.LayoutLength.InMeters,
                RecordDate = DateTime.Now,
                SessionKind = _lastDataSet.SessionInfo.SessionType,
                Simulator = _lastDataSet.Source,
                TrackFullName = _lastDataSet.SessionInfo.TrackInfo.TrackFullName,
                TraveledDistanceMeters = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.TraveledDistance.InMeters,
                ConsumedFuel = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ConsumedFuel,
                ElapsedSeconds = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime.TotalSeconds,
            };
            return currentSessionFuelConsumption;
        }

        private OverallFuelConsumptionHistory LoadOverallFuelConsumptionHistory()
        {
            return _fuelConsumptionRepository.LoadRatingsOrCreateNew();
        }
    }
}