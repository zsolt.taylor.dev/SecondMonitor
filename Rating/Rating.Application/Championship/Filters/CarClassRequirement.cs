﻿namespace SecondMonitor.Rating.Application.Championship.Filters
{
    using Common.DataModel.Championship;
    using DataModel.Snapshot;

    public class CarClassRequirement : IChampionshipCondition
    {
        public string GetDescription(ChampionshipDto championshipDto)
        {
            return championshipDto.ChampionshipState == ChampionshipState.NotStarted ? "Use class you would like to run the whole championship with." : $"Use one of the cars from class: {championshipDto.ClassName} ";
        }

        public ConditionResult Evaluate(ChampionshipDto championshipDto, SimulatorDataSet dataSet)
        {
            if (string.IsNullOrWhiteSpace(dataSet.PlayerInfo.CarClassName))
            {
                return new ConditionResult(RequirementResultKind.DoesNotMatch);
            }

            if (championshipDto.ChampionshipState == ChampionshipState.NotStarted)
            {
                return new ConditionResult(RequirementResultKind.CanMatch);
            }

            return dataSet.PlayerInfo.CarClassName == championshipDto.ClassName ? new ConditionResult(RequirementResultKind.PerfectMatch) : new ConditionResult(RequirementResultKind.DoesNotMatch, $"Current class is {dataSet.PlayerInfo.CarClassName}");
        }
    }
}