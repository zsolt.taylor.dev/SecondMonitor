﻿namespace SecondMonitor.RFactorConnector
{
    using System.Collections.Generic;
    using System.Linq;
    using AccConnector;
    using Contracts.NInject;
    using Ninject.Modules;

    public class AccConnectorModuleBootstrapper : INinjectModuleBootstrapper
    {
        public IList<INinjectModule> GetModules()
        {
            return new INinjectModule[] {new AccConnectorModule(), }.ToList();
        }
    }
}