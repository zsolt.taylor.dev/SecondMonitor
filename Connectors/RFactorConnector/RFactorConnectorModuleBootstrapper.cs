﻿namespace SecondMonitor.RFactorConnector
{
    using System.Collections.Generic;
    using System.Linq;
    using Contracts.NInject;
    using Ninject.Modules;

    public class RFactorConnectorModuleBootstrapper : INinjectModuleBootstrapper
    {
        public IList<INinjectModule> GetModules()
        {
            return new INinjectModule[] {new RFactorConnectorModule(), }.ToList();
        }
    }
}